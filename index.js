//BÀI 1
const KHU_VUC_A = "KVA";
const KHU_VUC_B = "KVB";
const KHU_VUC_C = "KVC";
const DOI_TUONG_1 = "DT1";
const DOI_TUONG_2 = "DT2";
const DOI_TUONG_3 = "DT3";

//handle Điểm cộng ưu tiên cho từng khu vực
function diemUuTienKV(UuTienKV) {
  switch (UuTienKV) {
    case KHU_VUC_A: {
      return 2;
    }
    case KHU_VUC_B: {
      return 1;
    }
    case KHU_VUC_C: {
      return 0.5;
    }
    default:
      return 0;
  }
}

//handle Điểm cộng ưu tiên cho từng đối tượng 
function diemUuTienDT(UuTienDT) {
  switch (UuTienDT) {
    case DOI_TUONG_1: {
      return 2.5;
    }
    case DOI_TUONG_2: {
      return 1.5;
    }
    case DOI_TUONG_3: {
      return 1;
    }
    default:
      return 0;
  }
}

//main
function handleKetQua() {
  console.log("yes");
  var diemChuan = document.getElementById("txt-diem-chuan").value * 1;
  var khuvucValue = document.getElementById("txt-khu-vuc").value;
  var doituongValue = document.getElementById("txt-doi-tuong").value;
  var diemMon1Value = document.getElementById("txt-diem-mon-1").value * 1;
  var diemMon2Value = document.getElementById("txt-diem-mon-2").value * 1;
  var diemMon3Value = document.getElementById("txt-diem-mon-3").value * 1;
  var tongDiemAll = 0;
  var bonusKhuVuc = diemUuTienKV(khuvucValue);
  var bonusDoiTuong = diemUuTienDT(doituongValue);
  var tongDiemAll =
    diemMon1Value + diemMon2Value + diemMon3Value + bonusDoiTuong + bonusKhuVuc;
  if (diemMon1Value == 0 || diemMon2Value == 0 || diemMon3Value == 0) {
    document.getElementById(
      "result-1"
    ).innerHTML = `<h2 class="text-warning">Bạn đã rớt. Do có điểm nhỏ hơn hoặc bằng 0</h2>`;
  } else if (
    (tongDiemAll >= diemChuan) & (diemMon1Value != 0) &&
    diemMon2Value != 0 &&
    diemMon3Value != 0
  ) {
    document.getElementById(
      "result-1"
    ).innerHTML = `<h2 class="text-secondary">Bạn đã đậu. Tổng điểm: ${tongDiemAll}</h2>`;
  } else {
    document.getElementById(
      "result-1"
    ).innerHTML = `<h2 class="text-warning" >Bạn đã rớt. Tổng điểm: ${tongDiemAll}</h2>`;
  }
}



//BÀI 2
const GIA_50KW_DAU = 500;
const GIA_50KW_KE = 650;
const GIA_100KW_KE = 850;
const GIA_150KW_KE = 1100;
const GIA_TIEN_CON_LAI = 1300;

//main
function handleTienDien() {
  console.log("ôke");
  var usernameValue =document.getElementById("txt-username").value;
  var soKwDaSuDung = document.getElementById("txt-so-kw").value * 1;
  var soTienPhaiThanhToan = 0;
      if(soKwDaSuDung <= 50){
    soTienPhaiThanhToan = soKwDaSuDung * GIA_50KW_DAU;
  }
  else if(soKwDaSuDung <= 100){
    soTienPhaiThanhToan = 50 * GIA_50KW_DAU +(soKwDaSuDung-50) * GIA_50KW_KE;
  }
  else if(soKwDaSuDung <= 200){
    soTienPhaiThanhToan = 50 * GIA_50KW_DAU + 50 * GIA_50KW_KE + (soKwDaSuDung - 100) * GIA_100KW_KE;
  }
  else if(soKwDaSuDung <= 350){
    soTienPhaiThanhToan = 50 * GIA_50KW_DAU + 50 * GIA_50KW_KE + 100 * GIA_100KW_KE + (soKwDaSuDung-200) * GIA_150KW_KE;
  }
  else{
    soTienPhaiThanhToan = 50 * GIA_50KW_DAU + 50 * GIA_150KW_KE + 100 * GIA_100KW_KE + 150 * GIA_150KW_KE +(soKwDaSuDung-350)*GIA_TIEN_CON_LAI;
  }
  document.getElementById("result-2").innerHTML=
  `<h2 class="text-secondary"> Họ & Tên : ${usernameValue}</h2>
  <h2 class="text-secondary">Tiền điện của quý khách là : ${soTienPhaiThanhToan.toLocaleString()} đ</h2>`
}
